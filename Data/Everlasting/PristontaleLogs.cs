﻿using System;
using System.Collections.Generic;

namespace LogViewer.Data.Everlasting
{
    public partial class PristontaleLogs
    {
        public int LogId { get; set; }
        public string Level { get; set; }
        public string CallSite { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string InnerException { get; set; }
        public string AdditionalInfo { get; set; }
        public string User { get; set; }
        public string RequestIp { get; set; }
        public string RequestHost { get; set; }
        public string RemoteAddress { get; set; }
        public DateTime LoggedOnDate { get; set; }
    }
}
