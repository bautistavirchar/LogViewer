﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LogViewer.Data.Server
{
    public partial class LogsContext : DbContext
    {
        public virtual DbSet<Dblagpnlogs> Dblagpnlogs { get; set; }

        public static string GetConnection { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(GetConnection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Dblagpnlogs>(entity =>
            {
                entity.HasKey(e => e.LogId);

                entity.ToTable("DBLAGPNLogs");

                entity.Property(e => e.AdditionalInfo).IsUnicode(false);

                entity.Property(e => e.CallSite)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.InnerException)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Level)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.LoggedOnDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.RemoteAddress)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.RequestHost)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.RequestIp)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.StackTrace)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.User)
                    .IsRequired()
                    .IsUnicode(false);
            });
        }
    }
}
