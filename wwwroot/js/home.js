$(function(){


    var currentDate = new Date(),
        m = currentDate.getMonth(),
        y = currentDate.getFullYear(),
        numOfDays = new Date(y, m+1, 0).getDate(),
        startDate = (m + 1) +'/1/'+ y,
        endDate = (m + 1) + '/' + numOfDays + '/' + y;

    $('#dateFrom').datetimepicker({
        viewMode: 'days',
        format: 'MM/DD/YYYY',
        defaultDate: startDate
    });

    $('#dateTo').datetimepicker({
        viewMode: 'days',
        format: 'MM/DD/YYYY',
        defaultDate: endDate
    });

    $table = $('#tblLogs').DataTable({
        lengthMenu: [[5, 10, 25, -1], [5, 10, 25, "All"]],
        processing: true,
        serverSide: true,
        autoWidth: false,
        deferRender: true,
        searchDelay: 400,
        dom: 'lfrtip',
        targets: 'no-sort',
        bSort: false,
        order: [],
        ajax: {
            'url': '/Home/GetLogs/',
            'type': 'POST',
            'dataType': 'JSON',
            'data': function(d){
                d.schema = $('#schema option:selected').val();
                d.logType = $('#logType option:selected').val();
                d.dateFrom = $('#dateFrom').val();
                d.dateTo = $('#dateTo').val();
            }
        },
        initComplete: function (settings, json) {
        },
        language: {
            processing: '<div><div id="div-processing" class="form-group"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div><div><strong>Loading...</strong></div></div>'
        },
        columns: [
            {'data': 'id'},
            {
                'data': null,
                'render': function (data, type, row) {
                    var color = data.level == 'Warn' ? 'warning' :
                                data.level == 'Info' ? 'info' : 'danger';

                    return '<span class="badge bg-'+ color +' ibadge">'+ data.level +'</span>'
                }
            },
            {'data': 'additionalInfo'},
            {'data': 'user'},
            {'data': 'loggedOnDate'},
            {
                'orderable': false,
                'data': null,
                'width': '5%',
                'render': function (data, type, row) {
                    var schemaType = $('#schema option:selected').val();
                    return '<a href="/Home/Log/'+ schemaType +'/'+ row.key +'" target="_blank" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> View</a>';
                }
            }
        ]
    });

    $(".dataTables_filter input")
    .unbind()
    .bind('keyup change', function (e) {
        if (e.keyCode == 13 || this.value == "") {
            $table.search(this.value).draw();
        }
    });
    $('.dataTables_length').before('<br><br>');
    $('#btnRefresh').on('click',function(){
        $table.ajax.reload();
    });


    $('.dropdown-control').change(function(){
        $table.ajax.reload();
    });
})