$(function(){
    $('#date').datetimepicker({
        viewMode: "months",  
        format: "MMMM YYYY",
        defaultDate: new Date()
    });
    var data = {};

    function loading(){
        var canvas = document.getElementById('myChart'),
            context = canvas.getContext('2d');
        
        context.clearRect(0,0,canvas.width,canvas.height);
        $('#loading').remove();
        $('#myChart').before('<div id="loading"><i class="fa fa-spinner fa-spinner-load fa-pulse fa-5x fa-fw"></i></div>');

    }

    function getChart(data, type){
        var canvas = document.getElementById('myChart');
        Chart.Line(canvas,{
            data:data,
            options: {
                // disabling curve
                elements: {
                    line: {
                        tension: 0
                    }
                },
                responsive: true,
                legend:{
                    display: false
                },
                title: {
                    display: true,
                    text: type.concat(' Log Reports')
                }
            }
        });
    }

    function ajaxData(color){
        $.ajax({
            url: '/Chart/GetDatas',
            type: 'GET',
            dataType: 'JSON',
            data: {
                schema: $('#schema option:selected').val(),
                type: $('#logType option:selected').val(),
                date:  moment($('#date').val()).format('MM/DD/YYYY')
            },
            beforeSend(){
                //
            },
            success: function(d){
                $('#loading').remove();
                data = {
                    labels: d.dates,
                    type: "line",
                    datasets: [
                        {
                            fill: false,
                            borderColor: color,
                            data: d.numbers
                        }
                    ]
                };
                getChart(data,$('#logType option:selected').val());
            }
        });
    }
    
    ajaxData('#ff9999');

    $('.dropdown-control').change(function(){
        loading();
        var val = $('#logType option:selected').val(),
            color = (val === 'Error') ? '#ff9999' :
                    (val === 'Warn') ? '#ffc299' : '#99ccff';
        ajaxData(color);   
    });

    $('#btnGo').click(function(){
        loading();
        var val = $('#logType option:selected').val(),
            color = (val === 'Error') ? '#ff9999' :
                    (val === 'Warn') ? '#ffc299' : '#99ccff';
        ajaxData(color);   
    });
    
});