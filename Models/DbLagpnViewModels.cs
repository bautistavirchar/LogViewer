namespace LogViewer.Models
{
    public class NLoggerViewModels
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string CallSite { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string InnerException { get; set; }
        public string AdditionalInfo { get; set; }
        public string Level { get; set; }
        public string User { get; set; }
        public string RequestIp { get; set; }
        public string RequestHost { get; set; }
        public string RemoteAddress { get; set; }
        public string LoggedOnDate { get; set; }
    }
}