using System;

namespace LogViewer.Models
{
    public class DatatablesParams
    {
        public int Start { get; set; }
        public int Length { get; set; }
        public string SearchKey { get; set; }
        public bool All { get; set; }
        public string LogType { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}