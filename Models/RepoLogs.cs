using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using LogViewer.Data;
using LogViewer.Utilities;
using Microsoft.EntityFrameworkCore;

namespace LogViewer.Models
{
    public class RepoLogs
    {
        private string _schema;
        public RepoLogs(string schema)
        {
            this._schema = schema;
        }
        public async Task<NLoggerViewModels> GetLagpnViewModelsAsync(int id){

            switch(_schema){
                case "Server": return await _Server();
                case "Everlasting": return await _Everlasting();
                case "LAGPNBranches": return await _LAGPNBranches();
                default: return await _Local();
            }
            
            async Task<NLoggerViewModels> _Local(){
                using(var db = new Data.Local.LogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    var row = await db.Dblagpnlogs.FindAsync(id);
                    return new NLoggerViewModels{
                        Id = row.LogId,
                        CallSite = row.CallSite,
                        Type = row.Type,
                        Message = row.Message,
                        StackTrace = row.StackTrace,
                        InnerException = row.InnerException,
                        AdditionalInfo = row.AdditionalInfo,
                        Level = row.Level,
                        User = row.User,
                        RequestIp = row.RequestIp,
                        RequestHost = row.RequestHost,
                        RemoteAddress = row.RemoteAddress,
                        LoggedOnDate = row.LoggedOnDate.ToString("MMM dd, yyyy hh:mm:ss tt")
                    };
                }
            }

            async Task<NLoggerViewModels> _Server(){
                using(var db = new Data.Server.LogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    var row = await db.Dblagpnlogs.FindAsync(id);
                    return new NLoggerViewModels{
                        Id = row.LogId,
                        CallSite = row.CallSite,
                        Type = row.Type,
                        Message = row.Message,
                        StackTrace = row.StackTrace,
                        InnerException = row.InnerException,
                        AdditionalInfo = row.AdditionalInfo,
                        Level = row.Level,
                        User = row.User,
                        RequestIp = row.RequestIp,
                        RequestHost = row.RequestHost,
                        RemoteAddress = row.RemoteAddress,
                        LoggedOnDate = row.LoggedOnDate.ToString("MMM dd, yyyy hh:mm:ss tt")
                    };
                }
            }

            async Task<NLoggerViewModels> _Everlasting(){
                using(var db = new Data.Everlasting.EverlastingLogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    var row = await db.PristontaleLogs.FindAsync(id);
                    return new NLoggerViewModels{
                        Id = row.LogId,
                        CallSite = row.CallSite,
                        Type = row.Type,
                        Message = row.Message,
                        StackTrace = row.StackTrace,
                        InnerException = row.InnerException,
                        AdditionalInfo = row.AdditionalInfo,
                        Level = row.Level,
                        User = row.User,
                        RequestIp = row.RequestIp,
                        RequestHost = row.RequestHost,
                        RemoteAddress = row.RemoteAddress,
                        LoggedOnDate = row.LoggedOnDate.ToString("MMM dd, yyyy hh:mm:ss tt")
                    };
                }
            }

            async Task<NLoggerViewModels> _LAGPNBranches(){
                using(var db = new Data.LAGPNBranches.LogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    var row = await db.LagpnbranchesLogs.FindAsync(id);
                    return new NLoggerViewModels{
                        Id = row.LogId,
                        CallSite = row.CallSite,
                        Type = row.Type,
                        Message = row.Message,
                        StackTrace = row.StackTrace,
                        InnerException = row.InnerException,
                        AdditionalInfo = row.AdditionalInfo,
                        Level = row.Level,
                        User = row.User,
                        RequestIp = row.RequestIp,
                        RequestHost = row.RequestHost,
                        RemoteAddress = row.RemoteAddress,
                        LoggedOnDate = row.LoggedOnDate.ToString("MMM dd, yyyy hh:mm:ss tt")
                    };
                }
            }
        
        }

        public async Task<(List<NLoggerViewModels> logResult, int count)> GetLogsASync(DatatablesParams param){
            switch(_schema){
                case "Server": return await _Server();
                case "Everlasting": return await _Everlasting();
                case "LAGPNBranches": return await _Lagpnbranches();
                default: return await _Local();
            }

            async Task<(List<NLoggerViewModels> logResult, int count)> _Local(){
                using(var db = new Data.Local.LogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    var query = Enumerable.Empty<Data.Local.Dblagpnlogs>().AsQueryable();
                    var result = new List<NLoggerViewModels>();
                    int count = 0;

                    try{
                        return await _GetLogsASync();
                    }finally{
                    }

                    async Task<(List<NLoggerViewModels> logResult, int count)> _GetLogsASync(){
                        var predicate = PredicateBuilder.True<Data.Local.Dblagpnlogs>();
                        predicate = predicate.And(x =>x.LoggedOnDate >= param.DateFrom && x.LoggedOnDate <= param.DateTo);                
                        
                        if(param.LogType != "All"){
                            predicate = predicate.And(x => x.Level == param.LogType && x.LoggedOnDate >= param.DateFrom && x.LoggedOnDate <= param.DateTo);   
                        }

                        if(param.All){
                            query = db.Dblagpnlogs.Where(predicate).OrderByDescending(x => x.LogId);
                            count = await query.CountAsync();
                        } else {
                            query = db.Dblagpnlogs.Where(predicate);
                            count = await query.CountAsync();
                            query = query.OrderByDescending(x => x.LogId).Skip(param.Start).Take(param.Length);
                        }

                        foreach(var log in query){
                            string additionalInfo = log.AdditionalInfo.Count() > 100 ? (log.AdditionalInfo.Substring(0,100) + "...") : log.AdditionalInfo;
                            result.Add(new NLoggerViewModels{
                                Id = log.LogId,
                                Key = log.LogId.ToString().EncryptString(),
                                CallSite = log.CallSite,
                                Type = log.Type,
                                Message = log.Message,
                                StackTrace = log.StackTrace,
                                InnerException = log.InnerException,
                                AdditionalInfo = additionalInfo,
                                Level = log.Level,
                                User = log.User,
                                RequestIp = log.RequestIp,
                                RequestHost = log.RequestHost,
                                RemoteAddress = log.RemoteAddress,
                                LoggedOnDate = log.LoggedOnDate.ToString("MMM dd, yyyy hh:mm:ss tt")
                            });
                        }

                        return (result,count);
                    }
                }
            }

            async Task<(List<NLoggerViewModels> logResult, int count)> _Server(){
                using(var db = new Data.Server.LogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    var query = Enumerable.Empty<Data.Server.Dblagpnlogs>().AsQueryable();
                    var result = new List<NLoggerViewModels>();
                    int count = 0;

                    try{
                        return await _GetLogsASync();
                    }finally{
                    }

                    async Task<(List<NLoggerViewModels> logResult, int count)> _GetLogsASync(){
                        var predicate = PredicateBuilder.True<Data.Server.Dblagpnlogs>();
                        predicate = predicate.And(x =>x.LoggedOnDate >= param.DateFrom && x.LoggedOnDate <= param.DateTo);                
                        
                        if(param.LogType != "All"){
                            predicate = predicate.And(x => x.Level == param.LogType && x.LoggedOnDate >= param.DateFrom && x.LoggedOnDate <= param.DateTo);   
                        }

                        if(param.All){
                            query = db.Dblagpnlogs.Where(predicate).OrderByDescending(x => x.LogId);
                            count = await query.CountAsync();
                        } else {
                            query = db.Dblagpnlogs.Where(predicate);
                            count = await query.CountAsync();
                            query = query.OrderByDescending(x => x.LogId).Skip(param.Start).Take(param.Length);
                        }

                        foreach(var log in query){
                            string additionalInfo = log.AdditionalInfo.Count() > 100 ? (log.AdditionalInfo.Substring(0,100) + "...") : log.AdditionalInfo;
                            result.Add(new NLoggerViewModels{
                                Id = log.LogId,
                                Key = log.LogId.ToString().EncryptString(),
                                CallSite = log.CallSite,
                                Type = log.Type,
                                Message = log.Message,
                                StackTrace = log.StackTrace,
                                InnerException = log.InnerException,
                                AdditionalInfo = additionalInfo,
                                Level = log.Level,
                                User = log.User,
                                RequestIp = log.RequestIp,
                                RequestHost = log.RequestHost,
                                RemoteAddress = log.RemoteAddress,
                                LoggedOnDate = log.LoggedOnDate.ToString("MMM dd, yyyy hh:mm:ss tt")
                            });
                        }

                        return (result,count);
                    }
                }
            }

            async Task<(List<NLoggerViewModels> logResult, int count)> _Everlasting(){
                using(var db = new Data.Everlasting.EverlastingLogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    var query = Enumerable.Empty<Data.Everlasting.PristontaleLogs>().AsQueryable();
                    var result = new List<NLoggerViewModels>();
                    int count = 0;

                    try{
                        return await _GetLogsASync();
                    }finally{
                    }

                    async Task<(List<NLoggerViewModels> logResult, int count)> _GetLogsASync(){
                        var predicate = PredicateBuilder.True<Data.Everlasting.PristontaleLogs>();
                        predicate = predicate.And(x =>x.LoggedOnDate >= param.DateFrom && x.LoggedOnDate <= param.DateTo);                
                        
                        if(param.LogType != "All"){
                            predicate = predicate.And(x => x.Level == param.LogType && x.LoggedOnDate >= param.DateFrom && x.LoggedOnDate <= param.DateTo);   
                        }

                        if(param.All){
                            query = db.PristontaleLogs.Where(predicate).OrderByDescending(x => x.LogId);
                            count = await query.CountAsync();
                        } else {
                            query = db.PristontaleLogs.Where(predicate);
                            count = await query.CountAsync();
                            query = query.OrderByDescending(x => x.LogId).Skip(param.Start).Take(param.Length);
                        }

                        foreach(var log in query){
                            string additionalInfo = log.AdditionalInfo.Count() > 100 ? (log.AdditionalInfo.Substring(0,100) + "...") : log.AdditionalInfo;
                            result.Add(new NLoggerViewModels{
                                Id = log.LogId,
                                Key = log.LogId.ToString().EncryptString(),
                                CallSite = log.CallSite,
                                Type = log.Type,
                                Message = log.Message,
                                StackTrace = log.StackTrace,
                                InnerException = log.InnerException,
                                AdditionalInfo = additionalInfo,
                                Level = log.Level,
                                User = log.User,
                                RequestIp = log.RequestIp,
                                RequestHost = log.RequestHost,
                                RemoteAddress = log.RemoteAddress,
                                LoggedOnDate = log.LoggedOnDate.ToString("MMM dd, yyyy hh:mm:ss tt")
                            });
                        }

                        return (result,count);
                    }
                }
            }
            
            async Task<(List<NLoggerViewModels> logResult, int count)> _Lagpnbranches(){
                using(var db = new Data.LAGPNBranches.LogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                    var query = Enumerable.Empty<Data.LAGPNBranches.LagpnbranchesLogs>().AsQueryable();
                    var result = new List<NLoggerViewModels>();
                    int count = 0;

                    try{
                        return await _GetLogsASync();
                    }finally{
                    }

                    async Task<(List<NLoggerViewModels> logResult, int count)> _GetLogsASync(){
                        var predicate = PredicateBuilder.True<Data.LAGPNBranches.LagpnbranchesLogs>();
                        predicate = predicate.And(x =>x.LoggedOnDate >= param.DateFrom && x.LoggedOnDate <= param.DateTo);                
                        
                        if(param.LogType != "All"){
                            predicate = predicate.And(x => x.Level == param.LogType && x.LoggedOnDate >= param.DateFrom && x.LoggedOnDate <= param.DateTo);   
                        }

                        if(param.All){
                            query = db.LagpnbranchesLogs.Where(predicate).OrderByDescending(x => x.LogId);
                            count = await query.CountAsync();
                        } else {
                            query = db.LagpnbranchesLogs.Where(predicate);
                            count = await query.CountAsync();
                            query = query.OrderByDescending(x => x.LogId).Skip(param.Start).Take(param.Length);
                        }

                        foreach(var log in query){
                            string additionalInfo = log.AdditionalInfo.Count() > 100 ? (log.AdditionalInfo.Substring(0,100) + "...") : log.AdditionalInfo;
                            result.Add(new NLoggerViewModels{
                                Id = log.LogId,
                                Key = log.LogId.ToString().EncryptString(),
                                CallSite = log.CallSite,
                                Type = log.Type,
                                Message = log.Message,
                                StackTrace = log.StackTrace,
                                InnerException = log.InnerException,
                                AdditionalInfo = additionalInfo,
                                Level = log.Level,
                                User = log.User,
                                RequestIp = log.RequestIp,
                                RequestHost = log.RequestHost,
                                RemoteAddress = log.RemoteAddress,
                                LoggedOnDate = log.LoggedOnDate.ToString("MMM dd, yyyy hh:mm:ss tt")
                            });
                        }

                        return (result,count);
                    }
                }
            }

            // ..
        }

        public async Task<object> GetChart(string level, DateTime date)
        {

            string  table = (_schema == "Everlasting") ? "dbo.PristontaleLogs" :
                            (_schema == "Server") ? "dbo.DBLAGPNLogs" :
                            (_schema == "LAGPNBranches") ? "dbo.LAGPNBranchesLogs" : "dbo.DBLAGPNLogs",
                    sql = $@"
                            select  convert(varchar, o.LoggedOnDate, 107) as [Date],
                                    count(o.Level) as [Count]
                            from    { table } as o
                            where   Level='{level}' and
                                    YEAR(o.LoggedOnDate) = '{ date.Year }' and
                                    MONTH(o.LoggedOnDate) = '{ date.Month }'
                                    group by convert(varchar, o.LoggedOnDate, 107)";

            switch(_schema){
                case "Server": return await _Server();
                case "Everlasting": return await _Everlasting();
                case "LAGPNBranches": return await _Lagpnbranches();
                default: return await _Local();
            }


            async Task<object> _Local(){
                using(var db = new Data.Local.LogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    var result =  await db.Database.GetDbConnection().QueryAsync<ChartModel>(sql);
                    var dates = new List<string>();
                    var numbers = new List<int>();
                    foreach(var c in result){
                        numbers.Add(c.Count);
                        dates.Add(c.Date);
                    }
                    return new {
                        dates = dates,
                        numbers = numbers
                    };
                }
            }

            async Task<object> _Server(){
                using(var db = new Data.Server.LogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    var result =  await db.Database.GetDbConnection().QueryAsync<ChartModel>(sql);
                    var dates = new List<string>();
                    var numbers = new List<int>();
                    foreach(var c in result){
                        numbers.Add(c.Count);
                        dates.Add(c.Date);
                    }
                    return new {
                        dates = dates,
                        numbers = numbers
                    };
                }
            }

            async Task<object> _Everlasting(){
                using(var db = new Data.Everlasting.EverlastingLogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    var result =  await db.Database.GetDbConnection().QueryAsync<ChartModel>(sql);
                    var dates = new List<string>();
                    var numbers = new List<int>();
                    foreach(var c in result){
                        numbers.Add(c.Count);
                        dates.Add(c.Date);
                    }
                    return new {
                        dates = dates,
                        numbers = numbers
                    };
                }
            }

            async Task<object> _Lagpnbranches(){
                using(var db = new Data.LAGPNBranches.LogsContext()){
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    var result =  await db.Database.GetDbConnection().QueryAsync<ChartModel>(sql);
                    var dates = new List<string>();
                    var numbers = new List<int>();
                    foreach(var c in result){
                        numbers.Add(c.Count);
                        dates.Add(c.Date);
                    }
                    return new {
                        dates = dates,
                        numbers = numbers
                    };
                }
            }

            // ...

        }
        
        // ..
    }
}