﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LogViewer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Data.Everlasting.EverlastingLogsContext.GetConnection = Configuration.GetConnectionString("EverlastingConnection");
            Data.Server.LogsContext.GetConnection = Configuration.GetConnectionString("ServerConnection");
            Data.Local.LogsContext.GetConnection = Configuration.GetConnectionString("LocalConnection");
            Data.LAGPNBranches.LogsContext.GetConnection = Configuration.GetConnectionString("LAGPNBranchesConnection");
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvcWithDefaultRoute();
            
            var setCulture = new CultureInfo("en-US");
            CultureInfo.DefaultThreadCurrentCulture = setCulture;
            CultureInfo.DefaultThreadCurrentUICulture = setCulture;
            CultureInfo.CurrentCulture = setCulture;
            CultureInfo.CurrentUICulture = setCulture;
        }
    }
}
