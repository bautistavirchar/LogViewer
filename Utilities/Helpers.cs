using System;
using simpleCipherStringPackage;

namespace LogViewer.Utilities
{
    static internal class Helpers
    {
        public static int ConvertToInt(this string value){
            if(int.TryParse(value, out var num)){
                return num;
            }
            return 0;
        }

        public static string EncryptString(this string value){
            var enc = new CipherString();
            return enc.EncryptData(value, nameof(LogViewer));
        }

        public static string DecryptString(this string value){
            try{
                var enc = new CipherString();
                return enc.DecryptData(value, nameof(LogViewer));
            }catch{
                return string.Empty;
            }
        }

        public static bool InvalidKey(this string cipher){
            try{
                var enc = new CipherString();
                return string.Equals(string.Empty, cipher.DecryptString(), StringComparison.Ordinal);
            }catch{
                return false;
            }
        }
    }
}