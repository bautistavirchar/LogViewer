using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LogViewer.Models;
using Microsoft.AspNetCore.Mvc;

namespace LogViewer.Controllers
{
    public class ChartController : Controller
    {
        public IActionResult Index() => View();

        public async Task<JsonResult> GetDatas(string schema, string type, DateTime date){
            var repo = new RepoLogs(schema);
            return Json(await repo.GetChart(type,date));
        }
    }
}