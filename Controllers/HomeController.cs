﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LogViewer.Models;
using LogViewer.Utilities;

namespace LogViewer.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

         [HttpPost]
        public async Task<JsonResult> GetLogs(string schema, string logType, DateTime dateFrom, DateTime dateTo)
        {
            string search = Request.Form["search[value]"],
                    start = Request.Form["start"];
            int length = Request.Form["length"].ToString().ConvertToInt();
            
            var param = new DatatablesParams{
                Start = start.ToString().ConvertToInt(),
                Length = length,
                All = (length == -1),
                SearchKey = search,
                LogType = logType,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            var repo = new RepoLogs(schema);
            var result = await repo.GetLogsASync(param);
            return Json(new {
                data = result.logResult,
                recordsTotal = result.count,
                recordsFiltered = result.count,
                param = new {
                    logType = param.LogType,
                    dateFrom = param.DateFrom,
                    dateTo = param.DateTo
                }
            });

        }

        [HttpGet("[controller]/Log/{schema}/{key}")]
        public async Task<IActionResult> Log(string schema, string key){
            if(key.InvalidKey()){
                return BadRequest();
            }
            var repo = new RepoLogs(schema);
            var result = await repo.GetLagpnViewModelsAsync(key.DecryptString().ConvertToInt());
            return View(result);
        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
